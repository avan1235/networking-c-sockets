# Networking C sockets

Project that shows a few examples of work with C sockets and implementing base networking features from scratch.
Every of the subprojects can be run independently and compiled using `Makefile` with `make` command.

# Project subprojects

## [UDP client-server](./udp-client-server)

Implementing communication between client and server using UDP protocol. Sending a specified number of chunks of specified bytes that are count by server.

## [TCP client-server](./tcp-client-server)

Implementing communication between client and server using TCP protocol. Sending a specified number of chunks of specified bytes that are count by server.

## [TCP file client-server](./tcp-file-client-server)

Implementing communication between client and server using TCP protocol. Sending file name, file size and file content from client to server using TCP.

## [Time client-server](./time-client-server)

Implementing communication using group address and mulicast address in order to localize time server in network and send the curretn time data from server to client (using UDP).

## [Poll server](./poll-server-telnet)

Implementing the poll server that not only supports sending data to it but also allows to connect to it via telnet connection that is a control connection which shows the current number of all connected clients

## [`libevent` file client-server](./libevent-file-client-server)

Implementing communication between client and server using TCP protocol with `libevent` library that allows to work in more abstract way.

## [Ping](./icmp-ping)

Implementing the ICMP protocol used in standard `ping` program. Sends data to specified address and measures the time of response.
