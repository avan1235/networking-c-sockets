#include <event2/event.h>
#include <event2/util.h>

#include <errno.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <unistd.h>

#include "err.h"

#define ACK "OK"
#define ACK_LEN 2

#define MAX_CLIENTS 16
#define BUF_SIZE 64

#define QUEUE_LENGTH     5
#define PORT_NUM     10001

#define DELIMITER '\n'


enum ReadFileState {
    READING_NAME,
    READING_SIZE,
    READING_CONTENT
};

struct connection_description {
    struct sockaddr_in address;
    evutil_socket_t sock;
    FILE *file;
    size_t meta_pos;
    char saved_name_buffer[FILENAME_MAX + 1];
    char meta_buffer[FILENAME_MAX + 1];
    enum ReadFileState read_state;
    size_t bytes_to_read;
    struct event *ev;
};

struct connection_description clients[MAX_CLIENTS];

void init_clients(void) {
    memset(clients, 0, sizeof(clients));
}

struct connection_description *get_client_slot(void) {
    int i;
    for (i = 0; i < MAX_CLIENTS; i++)
        if (!clients[i].ev)
            return &clients[i];
    return NULL;
}

void read_data(evutil_socket_t sock, struct connection_description *cl, const char *buf, int r, int s)
{
    for (int i = s; i < r; ++i) {
        cl->bytes_to_read -= 1;
        fputc(buf[i], cl->file);
    }

    if (cl->bytes_to_read <= 0) {
        fprintf(stdout, "File transfer complete, sending ACK.\n");
        if (fclose(cl->file) != 0) syserr("fclose");
        write(sock, ACK, ACK_LEN);
    }
}

void read_metadata(evutil_socket_t sock, struct connection_description *cl, const char *buf, int r, int i)
{
    while (cl->meta_pos < FILENAME_MAX && i < r && buf[i] != DELIMITER) {
        cl->meta_buffer[cl->meta_pos++] = buf[i++];
    }
    if (cl->meta_pos == FILENAME_MAX && buf[i] != DELIMITER) {
        fatal("Too big meta data");
    }
    if (buf[i] == DELIMITER) {
        cl->meta_buffer[cl->meta_pos] = 0;
        cl->meta_pos = 0;

        switch (cl->read_state) {

            case READING_NAME: {
                char *filename = cl->saved_name_buffer;
                memset(filename, 0, FILENAME_MAX);
                strcat(filename, "copy-");
                if (strlen(cl->meta_buffer) > FILENAME_MAX - strlen("copy-")) {
                    fatal("too long file name");
                }
                strcat(filename, cl->meta_buffer);
                cl->file = fopen(filename, "w+");

                cl->read_state = READING_SIZE;

                memset(cl->meta_buffer, 0, FILENAME_MAX + 1);
                if (i < r)
                    read_metadata(sock, cl, buf, r, i + 1);
            } break;
            case READING_SIZE: {
                char *endptr;
                cl->bytes_to_read = strtoul(cl->meta_buffer, &endptr, 10);
                cl->read_state = READING_CONTENT;
                memset(cl->meta_buffer, 0, FILENAME_MAX + 1);
                fprintf(stdout, "Waiting for file: %s [%zu bytes]\n", cl->saved_name_buffer, cl->bytes_to_read);
                if (i < r)
                    read_data(sock, cl, buf, r, i + 1);
            } break;
            case READING_CONTENT: {
                fatal("Got extra data after file content");
            } break;
        }
    }
    // next calback for data reading needed
}

void client_socket_cb(evutil_socket_t sock, short ev, void *arg) {
    struct connection_description *cl = (struct connection_description *) arg;
    char buf[BUF_SIZE + 1];

    int r = read(sock, buf, BUF_SIZE);
    if (r <= 0) {
        if (r < 0) {
            fprintf(stderr, "Error (%s) while reading data from %s:%d. Closing connection.\n",
                    strerror(errno), inet_ntoa(cl->address.sin_addr), ntohs(cl->address.sin_port));
        }
        else {
            fprintf(stderr, "Connection from %s:%d closed.\n",
                    inet_ntoa(cl->address.sin_addr), ntohs(cl->address.sin_port));
        }
        if (event_del(cl->ev) == -1) syserr("Can't delete the event.");
        event_free(cl->ev);
        if (close(sock) == -1) syserr("Error closing socket.");
        cl->ev = NULL;
        return;
    }
    buf[r] = 0;

    if (cl->read_state == READING_NAME || cl->read_state == READING_SIZE) {
        read_metadata(sock, cl, buf, r, 0);
    }
    else {
        read_data(sock, cl,  buf, r, 0);
    }
}

void listener_socket_cb(evutil_socket_t sock, short ev, void *arg) {
    struct event_base *base = (struct event_base *) arg;

    struct sockaddr_in sin;
    socklen_t addr_size = sizeof(struct sockaddr_in);
    evutil_socket_t connection_socket = accept(sock, (struct sockaddr *) &sin, &addr_size);

    if (connection_socket == -1) syserr("Error accepting connection.");

    struct connection_description *cl = get_client_slot();
    if (!cl) {
        close(connection_socket);
        fprintf(stderr, "Ignoring connection attempt from %s:%d due to lack of space.\n",
                inet_ntoa(sin.sin_addr), ntohs(sin.sin_port));
        return;
    }

    memcpy(&(cl->address), &sin, sizeof(struct sockaddr_in));
    cl->sock = connection_socket;

    struct event *an_event = event_new(base, connection_socket, EV_READ | EV_PERSIST, client_socket_cb, (void *) cl);
    if (!an_event) syserr("Error creating event.");
    cl->ev = an_event;
    cl->read_state = READING_NAME;
    cl->file = NULL;
    cl->meta_pos = 0;
    memset(cl->meta_buffer, 0, FILENAME_MAX + 1);
    memset(cl->saved_name_buffer, 0, FILENAME_MAX + 1);
    if (event_add(an_event, NULL) == -1) syserr("Error adding an event to a base.");
}

int main(int argc, char *argv[]) {
    struct event_base *base;

    init_clients();

    struct event_config *cfg = event_config_new();
    event_config_avoid_method(cfg, "epoll");
    base = event_base_new_with_config(cfg);
    event_config_free(cfg);

    if (!base) syserr("Error creating base.");

    evutil_socket_t listener_socket;
    listener_socket = socket(PF_INET, SOCK_STREAM, 0);
    if (listener_socket == -1 ||
        evutil_make_listen_socket_reuseable(listener_socket) ||
        evutil_make_socket_nonblocking(listener_socket)) {
        syserr("Error preparing socket.");
    }

    struct sockaddr_in sin;
    sin.sin_family = AF_INET;
    sin.sin_addr.s_addr = htonl(INADDR_ANY);
    sin.sin_port = htons(PORT_NUM);
    if (bind(listener_socket, (struct sockaddr *) &sin, sizeof(sin)) == -1) {
        syserr("bind");
    }

    if (listen(listener_socket, QUEUE_LENGTH) == -1) syserr("listen");

    struct event *listener_socket_event =
            event_new(base, listener_socket, EV_READ | EV_PERSIST, listener_socket_cb, (void *) base);
    if (!listener_socket_event) syserr("Error creating event for a listener socket.");

    if (event_add(listener_socket_event, NULL) == -1) syserr("Error adding listener_socket event.");

    printf("Entering dispatch loop.\n");
    if (event_base_dispatch(base) == -1) syserr("Error running dispatch loop.");
    printf("Dispatch loop finished.\n");

    event_free(listener_socket_event);
    event_base_free(base);

    return 0;
}
