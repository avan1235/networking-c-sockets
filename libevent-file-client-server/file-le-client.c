#include <event2/event.h>
#include <event2/buffer.h>
#include <event2/bufferevent.h>
#include <event2/util.h>

#include <errno.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <unistd.h>
#include <stdbool.h>

#include "err.h"

#define BUF_SIZE 4

#define ACK "OK"
#define ACK_LEN 2

#define DELIMITER "\n"

struct event_base *base;
struct bufferevent *bev;

struct file_description {
    FILE *file;
    char *file_name;
    size_t file_size;
    bool sent_file_meta;
    bool print_after_send;
};

void send_line(char *line)
{
    if (bufferevent_write(bev, line, strlen(line)) == -1)
        syserr("bufferevent_write");
    if (bufferevent_write(bev, DELIMITER, strlen(DELIMITER)) == -1)
        syserr("bufferevent_write");
}

void filein_cb(evutil_socket_t descriptor, short ev, void *arg) {

    struct file_description *desc = (struct file_description *) arg;

    unsigned char buf[BUF_SIZE+1];

    if (!desc->sent_file_meta) {
        send_line(desc->file_name);
        char file_size[64];
        snprintf(file_size, sizeof(file_size), "%zu", desc->file_size);
        send_line(file_size);
        desc->sent_file_meta = true;
    }

    int r = read(descriptor, buf, BUF_SIZE);
    if (r < 0)
        syserr("read (from stdin)");
    if (r == 0 && !desc->print_after_send) {
        printf("Sent full file.\n");
        desc->print_after_send = true;
        return;
    }
    if (bufferevent_write(bev, buf, r) == -1)
        syserr("bufferevent_write");
}

void a_read_cb(struct bufferevent *bev, void *arg) {
    char buf[ACK_LEN+1];

    while (evbuffer_get_length(bufferevent_get_input(bev))) {
        int r = bufferevent_read(bev, buf, BUF_SIZE);
        if (r == -1)
            syserr("bufferevent_read");
        buf[r] = 0;
        if (strcmp(ACK, buf) == 0) {
            printf("Received ACK.\n");
        }
        else {
            printf("Bad ACK received.\n");
        }
        if (event_base_loopbreak(base) == -1) syserr("event_base_loopbreak");
    }
}

void an_event_cb (struct bufferevent *bev, short what, void *arg) {
    if (what & BEV_EVENT_CONNECTED) {
        fprintf(stderr, "Connection made.\n");
        return;
    }
    if (what & BEV_EVENT_EOF)
        fprintf(stderr, "EOF encountered.\n");
    else if (what & BEV_EVENT_ERROR)
        fprintf(stderr, "Unrecoverable error.\n");
    else if (what & BEV_EVENT_TIMEOUT)
        fprintf(stderr, "A timeout occured.\n");
    if (event_base_loopbreak(base) == -1)
        syserr("event_base_loopbreak");
}

size_t get_file_size_by_name(const char *file_name)
{
    // open file for getting its size
    FILE *file = fopen(file_name, "r");
    if (!file)
        syserr("fopen");

    fseek(file, 0, SEEK_END);
    size_t size = ftell(file);
    fseek(file, 0, SEEK_SET);

    if (fclose(file) != 0)
        syserr("fclose");

    return size;
}

int main(int argc, char *argv[])
{
    /* Kontrola dokumentów ... */
    if (argc < 4)
        fatal("Usage: %s hostname port filename\n", argv[0]);

    FILE *file = fopen(argv[3], "r");
    if (!file)
        syserr("fopen");

    struct event_config *cfg = event_config_new();
    event_config_avoid_method(cfg, "epoll");
    base = event_base_new_with_config(cfg);
    event_config_free(cfg);

    if (!base)
        syserr("event_base_new");

    bev = bufferevent_socket_new(base, -1, BEV_OPT_CLOSE_ON_FREE);
    if (!bev)
        syserr("bufferevent_socket_new");
    bufferevent_setcb(bev, a_read_cb, NULL, an_event_cb, (void *)bev);

    struct addrinfo addr_hints = {
            .ai_flags = 0,
            .ai_family = AF_INET,
            .ai_socktype = SOCK_STREAM,
            .ai_protocol = 0,
            .ai_addrlen = 0,
            .ai_addr = NULL,
            .ai_canonname = NULL,
            .ai_next = NULL
    };
    struct addrinfo *addr;

    if (getaddrinfo(argv[1], argv[2], &addr_hints, &addr))
        syserr("getaddrinfo");

    if (bufferevent_socket_connect(bev, addr->ai_addr, addr->ai_addrlen) == -1)
        syserr("bufferevent_socket_connect");
    freeaddrinfo(addr);
    if (bufferevent_enable(bev, EV_READ | EV_WRITE) == -1)
        syserr("bufferevent_enable");

    struct file_description desc = {
            .file = file,
            .file_name = argv[3],
            .file_size = get_file_size_by_name(argv[3]),
            .sent_file_meta = false,
            .print_after_send = false
    };

    struct event *filein_event = event_new(base, fileno(file), EV_READ | EV_PERSIST, filein_cb, &desc);
    if (!filein_event)
        syserr("event_new");
    if (event_add(filein_event, NULL) == -1)
        syserr("event_add");

    printf("Entering dispatch loop.\n");
    if (event_base_dispatch(base) == -1)
        syserr("event_base_dispatch");
    printf("Dispatch loop finished.\n");

    bufferevent_flush(bev, EV_READ, BEV_FLUSH);
    bufferevent_free(bev);
    event_base_free(base);
    if (fclose(file) != 0)
        syserr("fclose");

    return 0;
}
