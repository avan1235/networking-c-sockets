#include <sys/types.h>
#include <sys/socket.h>
#include <sys/time.h>
#include <netdb.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <stdbool.h>
#include <stdlib.h>
#include <inttypes.h>
#include <netinet/ip_icmp.h>
#include <signal.h>
#include "err.h"

#define BSIZE 1000
#define ICMP_HEADER_LEN 8
#define TTL 64
#define RECV_TIMEOUT 1

unsigned short in_cksum(unsigned short *addr, uint16_t len);
void drop_to_nobody();
static void handle_sigint(int sig);
static inline uint64_t times_diff_u(struct timeval time_received, struct timeval time_sent);
static void send_ping_request(int sock, char *s_send_addr, int seq);
static int receive_ping_reply(int sock);
static void manage_delay(struct timeval *time, struct timeval *last_time, int i);

static bool working = true;


int main(int argc, char *argv[]) {
    int sock, ttl = TTL;
    struct timeval time, last_time;
    time.tv_sec = RECV_TIMEOUT;
    time.tv_usec = 0;
    int i = 0;

    if (argc < 2) fatal("Usage: %s host\n", argv[0]);

    signal(SIGINT, handle_sigint);
    sock = socket(AF_INET, SOCK_RAW, IPPROTO_ICMP);
    if (sock < 0) syserr("socket");
    if (setsockopt(sock, SOL_IP, IP_TTL, &ttl, sizeof(ttl)) != 0) syserr("setsockopt ttl");

    drop_to_nobody();

    do {
        manage_delay(&time, &last_time, i);
        send_ping_request(sock, argv[1], i++);
        gettimeofday(&time, NULL); // for delaying the send request for about every RECV_TIMEOUT seconds
        receive_ping_reply(sock);
    } while (working);

    if (close(sock) == -1) syserr("close");

    return 0;
}

static void handle_sigint(int sig)
{
    working = false;
}

static inline uint64_t times_diff_u(struct timeval time_received, struct timeval time_sent)
{
    return (time_received.tv_sec - time_sent.tv_sec) * 1e6 + (time_received.tv_usec - time_sent.tv_usec);
}

static void send_ping_request(int sock, char *s_send_addr, int seq)
{
    struct addrinfo addr_hints;
    struct addrinfo *addr_result;
    struct sockaddr_in send_addr;
    struct icmp *icmp;
    char send_buffer[BSIZE];
    struct timeval time;
    int err = 0;
    ssize_t data_len = 0;
    ssize_t icmp_len = 0;
    ssize_t len = 0;

    // 'converting' host/port in string to struct addrinfo
    memset(&addr_hints, 0, sizeof(struct addrinfo));
    addr_hints.ai_family = AF_INET;
    addr_hints.ai_socktype = SOCK_RAW;
    addr_hints.ai_protocol = IPPROTO_ICMP;
    err = getaddrinfo(s_send_addr, 0, &addr_hints, &addr_result);
    if (err != 0) {
        syserr("getaddrinfo: %s\n", gai_strerror(err));
    }

    send_addr.sin_family = AF_INET;
    send_addr.sin_addr.s_addr = ((struct sockaddr_in *) (addr_result->ai_addr))->sin_addr.s_addr;
    send_addr.sin_port = htons(0);
    freeaddrinfo(addr_result);

    memset(send_buffer, 0, sizeof(send_buffer));
    // initializing ICMP header
    icmp = (struct icmp *) send_buffer;
    icmp->icmp_type = ICMP_ECHO;
    icmp->icmp_code = 0;
    icmp->icmp_id = htons(getpid()); // process identified by PID
    icmp->icmp_seq = htons(seq); // sequential number

    gettimeofday(&time, NULL);
    data_len = snprintf(((char *) send_buffer + ICMP_HEADER_LEN), sizeof(send_buffer) - ICMP_HEADER_LEN, "%lds%ldus", time.tv_sec, time.tv_usec);
    if (data_len < 1) {
        syserr("snprintf");
    }
    icmp_len = data_len + ICMP_HEADER_LEN; // packet is filled with 0
    icmp->icmp_cksum = 0; // checksum computed over whole ICMP package
    icmp->icmp_cksum = in_cksum((unsigned short *) icmp, icmp_len);

    len = sendto(sock, (void *) icmp, icmp_len, 0, (struct sockaddr *) &send_addr, (socklen_t) sizeof(send_addr));
    if (icmp_len != (ssize_t) len) {
        syserr("partial / failed write");
    }
}

static int receive_ping_reply(int sock)
{
    struct sockaddr_in rcv_addr;
    socklen_t rcv_addr_len;
    struct timeval time_sent;
    struct timeval time_received;

    struct ip *ip;
    struct icmp *icmp;
    uint64_t us_diff;
    int ms, us;
    char rcv_buffer[BSIZE];

    ssize_t ip_header_len = 0;
    ssize_t icmp_len = 0;
    ssize_t len;

    memset(rcv_buffer, 0, sizeof(rcv_buffer));
    rcv_addr_len = (socklen_t)
    sizeof(rcv_addr);
    len = recvfrom(sock, (void *) rcv_buffer, sizeof(rcv_buffer), 0, (struct sockaddr *) &rcv_addr, &rcv_addr_len);
    gettimeofday(&time_received, NULL);

    if (len == -1) {
        syserr("failed read");
    }

    // recvfrom returns whole packet (with IP header)
    ip = (struct ip *) rcv_buffer;
    ip_header_len = ip->ip_hl << 2; // IP header len is in 4-byte words

    icmp = (struct icmp *) (rcv_buffer + ip_header_len); // ICMP header follows IP
    icmp_len = len - ip_header_len;

    if (icmp_len < ICMP_HEADER_LEN) {
        fatal("icmp header len (%d) < ICMP_HEADER_LEN", icmp_len);
    }

    if (icmp->icmp_type != ICMP_ECHOREPLY) {
        fatal("strange reply type (%d)\n", icmp->icmp_type);
    }

    if (ntohs(icmp->icmp_id) != getpid()) {
        fatal("reply with id %d different from my pid %d", ntohs(icmp->icmp_id), getpid());
    }

    if (sscanf((rcv_buffer + ip_header_len + ICMP_HEADER_LEN), "%lds%ldus", &time_sent.tv_sec, &time_sent.tv_usec) != 2) {
        fatal("invalid data format received");
    }

    us_diff = times_diff_u(time_received, time_sent);
    ms = us_diff / 1000;
    us = us_diff % 1000;
    printf("%zd bytes from %s icmp_seq=%d ttl=%d time=%d.%dms\n", len, inet_ntoa(rcv_addr.sin_addr), ntohs(icmp->icmp_seq), ip->ip_ttl, ms, us);

    return 1;
}

static void manage_delay(struct timeval *time, struct timeval *last_time, int i)
{
    useconds_t remained;
    if (i > 0) {
        *last_time = *time;
        gettimeofday(time, NULL);
        remained = RECV_TIMEOUT * 1e6 - times_diff_u(*time, *last_time);
        if (remained > 0) {
            usleep(remained);
        }
    }
}


