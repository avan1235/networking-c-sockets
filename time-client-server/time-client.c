#include <stdlib.h>
#include <stdio.h>
#include <unistd.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <poll.h>
#include <stdbool.h>
#include <string.h>

#include "err.h"

#define BSIZE           256
#define TTL_VALUE       4
#define REPEAT_COUNT    3
#define TIMEOUT         3000
#define GET_TIME        "GET TIME"

int main(int argc, char *argv[]) {

    char *group_address;
    struct ip_mreq ip_mreq;
    in_port_t port;
    int sock, optval;
    struct sockaddr_in address;
    struct sockaddr server;
    struct sockaddr sender;
    socklen_t sendsize = sizeof(sender);
    ssize_t rcv_len;
    char buffer[BSIZE];
    int i;
    struct pollfd fd;
    int ret;

    if (argc != 3) {
        fatal("Usage: %s group_address server_port\n", argv[0]);
    }

    group_address = argv[1];
    port = (in_port_t) atoi(argv[2]);

    sock = socket(AF_INET, SOCK_DGRAM, 0);
    if (sock < 0) {
        syserr("socket");
    }

    optval = 1;
    if (setsockopt(sock, SOL_SOCKET, SO_BROADCAST, (void *) &optval, sizeof optval) < 0) {
        syserr("setsockopt broadcast");
    }

    optval = TTL_VALUE;
    if (setsockopt(sock, IPPROTO_IP, IP_MULTICAST_TTL, (void *) &optval, sizeof optval) < 0) {
        syserr("setsockopt multicast ttl");
    }

    optval = 1;
    if (setsockopt(sock, SOL_SOCKET, SO_REUSEADDR, &optval, sizeof optval) < 0) {
        perror("reuseaddr setsockopt");
        exit(1);
    }

    address.sin_family = AF_INET;
    address.sin_port = htons(port);
    if (inet_aton(group_address, &address.sin_addr) == 0) {
        fprintf(stderr, "ERROR: inet_aton - invalid multicast address\n");
        exit(EXIT_FAILURE);
    }

    socklen_t size = sizeof(server);
    getsockname(sock, &server, &size);

    ip_mreq.imr_interface.s_addr = htonl(INADDR_ANY);
    if (inet_aton(group_address, &ip_mreq.imr_multiaddr) == 0) {
        fprintf(stderr, "ERROR: inet_aton - invalid multicast address\n");
        exit(EXIT_FAILURE);
    }

    if (setsockopt(sock, IPPROTO_IP, IP_ADD_MEMBERSHIP, (void *) &ip_mreq, sizeof ip_mreq) < 0) {
        syserr("setsockopt");
    }

    bool loop = true;
    if (setsockopt(sock, IPPROTO_IP, IP_MULTICAST_LOOP, (void *) &loop, sizeof loop) < 0) {
        syserr("setsockopt");
    }

    bool got_message = false;
    fd.fd = sock;
    fd.events = POLLIN;
    fd.revents = 0;
    for (i = 0; i < REPEAT_COUNT && !got_message; ++i) {
        if (sendto(sock, GET_TIME, strlen(GET_TIME), 0, (struct sockaddr *) &address, sizeof(address)) != strlen(GET_TIME)) {
            syserr("sendto");
        }
        printf("Sending request[%d]\n", i + 1);

        ret = poll(&fd, 1, TIMEOUT);
        switch (ret) {
            case -1:
                syserr("error in poll");
                break;
            case 0:
                // let's try again as the request timeouted
                break;
            default: {
                rcv_len = recvfrom(sock, buffer, BSIZE, 0, &sender, &sendsize);
                if (rcv_len < 0) {
                    syserr("read");
                }
                else {
                    buffer[rcv_len] = '\0';
                    printf("Response from: %s\n", inet_ntoa(((struct sockaddr_in *) &sender)->sin_addr));
                    printf("Received time: %s\n", buffer);
                    got_message = true;
                }
            } break;
        }
    }

    if (!got_message) {
        printf("Timeout: unable to receive response.\n");
    }

    close(sock);
    exit(EXIT_SUCCESS);
}
