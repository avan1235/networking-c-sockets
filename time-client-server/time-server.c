#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <time.h>
#include <stdbool.h>

#include "err.h"

#define BSIZE           256
#define GET_TIME        "GET TIME"

int main(int argc, char *argv[]) {

    char *group_address;
    in_port_t port;
    int sock;
    struct sockaddr_in address;
    struct sockaddr sender;
    socklen_t send_size = sizeof(sender);
    struct ip_mreq ip_mreq;
    time_t time_buffer;
    char buffer[BSIZE + 1];
    ssize_t rcv_len;

    if (argc != 3) {
        fatal("Usage: %s group_address server_port\n", argv[0]);
    }

    group_address = argv[1];
    port = (in_port_t) atoi(argv[2]);

    sock = socket(AF_INET, SOCK_DGRAM, 0);
    if (sock < 0) {
        syserr("socket");
    }

    ip_mreq.imr_interface.s_addr = htonl(INADDR_ANY);
    if (inet_aton(group_address, &ip_mreq.imr_multiaddr) == 0) {
        fprintf(stderr, "ERROR: inet_aton - invalid multicast address\n");
        exit(EXIT_FAILURE);
    }

    if (setsockopt(sock, IPPROTO_IP, IP_ADD_MEMBERSHIP, (void *) &ip_mreq, sizeof ip_mreq) < 0) {
        syserr("setsockopt");
    }

    address.sin_family = AF_INET;
    address.sin_addr.s_addr = htonl(INADDR_ANY);
    address.sin_port = htons(port);
    if (bind(sock, (struct sockaddr *) &address, sizeof address) < 0) {
        syserr("bind");
    }

    while (true) {
        rcv_len = recvfrom(sock, buffer, BSIZE, 0, &sender, &send_size);
        if (rcv_len < 0) {
            syserr("read");
        }
        else {
            buffer[rcv_len] = '\0';

            if (strcmp(GET_TIME, buffer) == 0) {
                time(&time_buffer);
                strncpy(buffer, ctime(&time_buffer), BSIZE);
                printf("Request from: %s\n", inet_ntoa(((struct sockaddr_in *) &sender)->sin_addr));

                if (sendto(sock, buffer, strlen((buffer)), 0, &sender, send_size) < 0) {
                    syserr("sendto");
                }
            }
            else {
                printf("Received unknown command: %s\n", buffer);
            }
        }
    }

    if (setsockopt(sock, IPPROTO_IP, IP_DROP_MEMBERSHIP, (void *) &ip_mreq, sizeof ip_mreq) < 0) {
        syserr("setsockopt");
    }

    close(sock);
    exit(EXIT_SUCCESS);
}
