#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <stdio.h>
#include <string.h>
#include <unistd.h>
#include <stdlib.h>

#include "err.h"

int main(int argc, char *argv[])
{
  int sock;
  struct addrinfo addr_hints;
  struct addrinfo *addr_result;

  int i, err;

  if (argc != 5) {
    fatal("Usage: %s host port n k\n", argv[0]);
  }

  // 'converting' host/port in string to struct addrinfo
  memset(&addr_hints, 0, sizeof(struct addrinfo));
  addr_hints.ai_family = AF_INET; // IPv4
  addr_hints.ai_socktype = SOCK_STREAM;
  addr_hints.ai_protocol = IPPROTO_TCP;
  err = getaddrinfo(argv[1], argv[2], &addr_hints, &addr_result);
  if (err == EAI_SYSTEM) { // system error
    syserr("getaddrinfo: %s", gai_strerror(err));
  }
  else if (err != 0) { // other error (host not found, etc.)
    fatal("getaddrinfo: %s", gai_strerror(err));
  }

  // initialize socket according to getaddrinfo results
  sock = socket(addr_result->ai_family, addr_result->ai_socktype, addr_result->ai_protocol);
  if (sock < 0)
    syserr("socket");

  // connect socket to the server
  if (connect(sock, addr_result->ai_addr, addr_result->ai_addrlen) < 0)
    syserr("connect");

  freeaddrinfo(addr_result);

  uint32_t n = (uint32_t)atoi(argv[3]);
  uint32_t k = (uint32_t)atoi(argv[4]);
  char *send_data = (char *)malloc((k + 1) * sizeof(char));
  (void)memset(send_data, '$', k);
  send_data[k] = 0; // let's make a string from this allocated memory

  for (i = 0; i < n; i++) {
    printf("writing to socket: %s\n", send_data);
    if (write(sock, send_data, k) != k) {
      syserr("partial / failed write");
    }
  }

  (void) close(sock); // socket would be closed anyway when the program ends

  return 0;
}
