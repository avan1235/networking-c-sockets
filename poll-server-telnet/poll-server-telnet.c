#include <limits.h>
#include <poll.h>
#include <signal.h>
#include <stdio.h>
#include <string.h>
#include <sys/socket.h>
#include <netdb.h>
#include <unistd.h>
#include <stdlib.h>
#include <errno.h>
#include "err.h"

#define TRUE 1
#define FALSE 0
#define BUF_SIZE 1024

#define MAX_PORT 65535

#define MAX_QUEUE 5

static int finish = FALSE;

static const char *control_message = "count";

static void catch_int(int sig)
{
    finish = TRUE;
    fprintf(stderr,"Signal %d catched. No new connections will be accepted.\n", sig);
}

int parse_port(char *port_str)
{
    long port;
    char *endptr;

    errno = 0;
    port = strtol(port_str, &endptr, 10);

    /* Check for various possible errors */

    if ((errno == ERANGE && (port == LONG_MAX || port == LONG_MIN)) || (errno != 0 && port == 0)) {
        syserr("strtol");
    }

    if (endptr == port_str) {
        fatal("No digits were found\n");
    }

    if (port < 0 || port > MAX_PORT) {
        fatal("bad port range given");
    }

    return (int) port; // range matches int range
}

void create_listen_server(int port, struct pollfd *fd, char *use_case)
{
    size_t length;
    struct sockaddr_in server;

    // specyfikujemy adres, na którym będzie działał serwer
    server.sin_family = AF_INET;
    server.sin_addr.s_addr = htonl(INADDR_ANY);
    server.sin_port = htons(port);
    if (bind(fd->fd, (struct sockaddr *) &server, (socklen_t) sizeof(server)) == -1) {
        syserr("Binding stream socket");
    }

    length = sizeof(server);
    if (getsockname(fd->fd, (struct sockaddr *) &server, (socklen_t *) &length) == -1) {
        syserr("Getting socket name for server input");
    }
    printf("Socket port #%u used for %s\n", (unsigned) ntohs(server.sin_port), use_case);

    if (listen(fd->fd, MAX_QUEUE) == -1) {
        syserr("Starting to listen");
    }
}

void close_fd_if_opened(int *fd)
{
    if (*fd >= 0) {
        if (close(*fd) < 0) {
            syserr("close");
        }
        *fd = -1;
    }
}

int main(int argc, char *argv[]) {
    if (argc != 3) {
        fatal("Usage: %s <clients-port> <control-port>", argv[0]);
    }

    struct pollfd client[_POSIX_OPEN_MAX];
    int used_as_control[_POSIX_OPEN_MAX]; // 1 gedziemy zaznaczac tych clientow, którzy podlaczyli sie na port kontrolny
    char buf[BUF_SIZE];
    size_t rval;
    int msgsock, activeClients, activeClientsControl, totalClients, i, ret;
    struct sigaction action;
    sigset_t block_mask;

    int port_in = parse_port(argv[1]);
    int port_contr = parse_port(argv[2]);

    fprintf(stderr, "_POSIX_OPEN_MAX = %d\n", _POSIX_OPEN_MAX);

    // obsługa sygnału po Ctrl-C
    sigemptyset(&block_mask);
    action.sa_handler = catch_int;
    action.sa_mask = block_mask;
    action.sa_flags = SA_RESTART;

    if (sigaction(SIGINT, &action, 0) == -1) {
        syserr("sigaction");
    }

    for (i = 0; i < _POSIX_OPEN_MAX; ++i) {
        used_as_control[i] = FALSE;
    }

    // Inicjujemy tablicę z gniazdkami klientów
    // client[0] to gniazdo centrali
    // client[1] to gniazdo kontrolne
    for (i = 0; i < _POSIX_OPEN_MAX; ++i) {
        client[i].fd = -1;
        client[i].events = POLLIN;
        client[i].revents = 0;
    }
    activeClients = 0;
    activeClientsControl = 0;
    totalClients = 0;


    // fd dla server do przyjmowania klientów
    client[0].fd = socket(PF_INET, SOCK_STREAM, 0);
    if (client[0].fd == -1) {
        syserr("Opening stream socket main");
    }

    // fd dla server jako port kontrolny
    client[1].fd = socket(PF_INET, SOCK_STREAM, 0);
    if (client[1].fd == -1) {
        syserr("Opening stream socket control");
    }

    create_listen_server(port_in, &client[0], "clients");
    create_listen_server(port_contr, &client[1], "control");

    do {
        for (i = 0; i < _POSIX_OPEN_MAX; ++i) {
            client[i].revents = 0;
        }

        // kończymy po Ctrl-C zamykając gniazda
        if (finish == TRUE) {
            close_fd_if_opened(&client[0].fd); // nasłuchiwanie klientów
            close_fd_if_opened(&client[1].fd); // obsługiwanie kontrolne - czekanie na zgłoszenie
        }

        // czekamy do skutku az pojawi się coś na którym fd
        ret = poll(client, _POSIX_OPEN_MAX, -1);
        if (ret == -1) {
            if (errno == EINTR) {
                fprintf(stderr, "Interrupted system call\n");
            }
            else {
                syserr("poll");
            }
        }
        else if (ret > 0) {
            if (finish == FALSE && (client[0].revents & POLLIN)) {
               // Przyjmujemy nowe połaczenie klienta
                msgsock = accept(client[0].fd, (struct sockaddr *) 0, (socklen_t *) 0);
                if (msgsock == -1) {
                    syserr("accept");
                }
                else {
                    for (i = 2; i < _POSIX_OPEN_MAX; ++i) {
                        if (client[i].fd == -1) {
                            fprintf(stderr, "Received new connection (%d)\n", i);
                            client[i].fd = msgsock;
                            client[i].events = POLLIN;
                            used_as_control[i] = FALSE;
                            activeClients += 1;
                            totalClients += 1;
                            break;
                        }
                    }
                    if (i >= _POSIX_OPEN_MAX) {
                        fprintf(stderr, "Too many clients\n");
                        if (close(msgsock) < 0) {
                            syserr("close");
                        }
                    }
                }
            }

            if (finish == FALSE && (client[1].revents & POLLIN)) {
                // Przyjmujemy nowe połaczenie kontrolne
                msgsock = accept(client[1].fd, (struct sockaddr *) 0, (socklen_t *) 0);
                if (msgsock == -1) {
                    syserr("accept");
                }
                else {
                    for (i = 2; i < _POSIX_OPEN_MAX; ++i) {
                        if (client[i].fd == -1) {
                            fprintf(stderr, "Received new control connection (%d)\n", i);
                            client[i].fd = msgsock;
                            client[i].events = POLLIN;
                            used_as_control[i] = TRUE;
                            activeClientsControl += 1;
                            break;
                        }
                    }
                    if (i >= _POSIX_OPEN_MAX) {
                        fprintf(stderr, "Too many control clients\n");
                        if (close(msgsock) < 0) {
                            syserr("close");
                        }
                    }
                }
            }

            for (i = 2; i < _POSIX_OPEN_MAX; ++i) {
                if (client[i].fd != -1 && (client[i].revents & (POLLIN | POLLERR))) {
                    if (used_as_control[i]) {
                        rval = read(client[i].fd, buf, strlen(control_message));
                        if (rval >= strlen(control_message)) {
                            // bierzemy pod uwagę jedynie pierwsze 5 znaków
                            buf[5] = '\0';
                            if (strcmp(control_message, buf) == 0) {
                                // liczby klientów  są na tyle małymi liczbami że komunikat na pewno zmieści się w buforze
                                snprintf(buf, BUF_SIZE,
                                         "Number of active clients: %d\n"
                                         "Total number of clients: %d\n", activeClients, totalClients);

                                if (write(client[i].fd, buf, strlen(buf)) < 0) {
                                    syserr("Writing on control socket");
                                }
                            }
                        }
                        else if (rval < 0) {
                            fprintf(stderr, "Reading control message error (%d, %s)\n", errno, strerror(errno));
                        }
                        fprintf(stderr, "Ending control connection\n");
                        if (close(client[i].fd) < 0) {
                            syserr("close");
                        }
                        client[i].fd = -1;
                        activeClientsControl -= 1;
                    }
                    else {
                        rval = read(client[i].fd, buf, BUF_SIZE);
                        if (rval < 0) {
                            fprintf(stderr, "Reading message (%d, %s)\n", errno, strerror(errno));
                            if (close(client[i].fd) < 0) {
                                syserr("close");
                            }
                            client[i].fd = -1;
                            activeClients -= 1;
                        }
                        else if (rval == 0) {
                            fprintf(stderr, "Ending connection\n");
                            if (close(client[i].fd) < 0) {
                                syserr("close");
                            }
                            client[i].fd = -1;
                            activeClients -= 1;
                        }
                        else {
                            printf("-->%.*s\n", (int) rval, buf);
                        }
                    }
                }
            }
        }
        else {
            fatal("Should not happen as we wait without timeout");
        }
    } while (finish == FALSE || activeClients + activeClientsControl > 0);

    close_fd_if_opened(&client[0].fd);
    close_fd_if_opened(&client[1].fd);

    exit(EXIT_SUCCESS);
}
