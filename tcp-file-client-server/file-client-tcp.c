/*
 Program uruchamiamy z dwoma parametrami: nazwa serwera i numer jego portu.
 Program spróbuje połączyć się z serwerem, po czym będzie od nas pobierał
 linie tekstu i wysyłał je do serwera.  Wpisanie BYE kończy pracę.
*/

#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netdb.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include "err.h"

#define BUFFER_SIZE      10240

char *get_name_from_path(char *path) {
    int idx = -1;
    int i = 0;
    for (; path[i] != 0; ++i) {
        if (path[i] == '/') {
            idx = i;
        }
    }

    return path + idx + 1;
}

long get_file_size(FILE *file) {
    fseek(file, 0, SEEK_END);
    long size = ftell(file);
    fseek(file, 0, SEEK_SET);
    return size;
}


int main(int argc, char *argv[]) {
    FILE *file = NULL;
    int rc;
    int sock;
    struct addrinfo addr_hints, *addr_result;
    char *filename;
    char buffer[BUFFER_SIZE];
    unsigned long filesize;

    if (argc != 4) {
        fatal("Usage: %s host port filename", argv[0]);
    }

    sock = socket(PF_INET, SOCK_STREAM, IPPROTO_TCP);
    if (sock < 0) {
        syserr("socket");
    }

    memset(&addr_hints, 0, sizeof(struct addrinfo));
    addr_hints.ai_flags = 0;
    addr_hints.ai_family = AF_INET;
    addr_hints.ai_socktype = SOCK_STREAM;
    addr_hints.ai_protocol = IPPROTO_TCP;

    rc = getaddrinfo(argv[1], argv[2], &addr_hints, &addr_result);
    if (rc != 0) {
        fprintf(stderr, "rc=%d\n", rc);
        syserr("getaddrinfo: %s", gai_strerror(rc));
    }

    if (connect(sock, addr_result->ai_addr, addr_result->ai_addrlen) != 0) {
        syserr("connect");
    }
    freeaddrinfo(addr_result);

    file = fopen(argv[3], "r");
    if (file == NULL) {
        syserr("fopen");
    }

    filename = get_name_from_path(argv[3]);
    filesize = get_file_size(file);

    // tworzymy jednolinijkowe napisy z wyciągniętych właściwości z pliku, które wyślemy
    char filesize_str[255];
    sprintf(filesize_str, "%ld\n", filesize);
    char *filename_newline = (char *) malloc((strlen(filename) + 2) * sizeof(char));
    strcpy(filename_newline, filename);
    filename_newline[strlen(filename)] = '\n';
    filename_newline[strlen(filename) + 1] = 0;

    if (write(sock, filename_newline, strlen(filename_newline)) < 0) {
        syserr("writing on stream socket");
    }

    if (write(sock, filesize_str, strlen(filesize_str)) < 0) {
        syserr("writing on stream socket");
    }

    unsigned long sent_bytes;
    /* Sending file data */
    while ((sent_bytes = fread(buffer, sizeof(char), BUFFER_SIZE, file)) > 0) {
        if (write(sock, buffer, sent_bytes) < 0) {
            syserr("writing on stream socket");
        }
    }

    if (fclose(file) < 0) {
        syserr("closing file socket");
    }

    if (close(sock) < 0) {
        syserr("closing stream socket");
    }

    return 0;
}

