#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <time.h>
#include <unistd.h>
#include <pthread.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "err.h"

#define BUFFER_SIZE 10240
#define FILENAME_SIZE 1024
#define LINE_SIZE 128

const struct timespec millis_100 = { .tv_sec = 0, .tv_nsec = 1e8 };
const struct timespec millis_sec = { .tv_sec = 1, .tv_nsec = 0 };

pthread_mutex_t lock;
unsigned long total_upload = 0;

void *handle_connection(void *s_ptr) {
    int ret, s;
    socklen_t len;
    char buffer[BUFFER_SIZE + 1], peername[LINE_SIZE + 1], peeraddr[LINE_SIZE + 1];
    struct sockaddr_in addr;

    s = *(int *) s_ptr;
    free(s_ptr);

    len = sizeof(addr);

    /**
     * potrzeba chwile odczekac przed odebraniem pierwszego komunikatu wysyłanego
     * przez klienta, ponieważ w przeciwnym razie (braku odczekania) zdarzają
     * sie przypadki, że pierwszego komunikatu nie ma w buforze
     */
    struct timespec remain;
    nanosleep(&millis_100, &remain);

    ret = getpeername(s, (struct sockaddr *) &addr, &len);
    if (ret == -1)
        syserr("getsockname");

    inet_ntop(AF_INET, &addr.sin_addr, peeraddr, BUFFER_SIZE);
    snprintf(peername, 2 * BUFFER_SIZE, "%s:%d", peeraddr, ntohs(addr.sin_port));

    char filename[FILENAME_SIZE + 1];
    memset(buffer, 0, sizeof(buffer));
    memset(filename, 0, sizeof(filename));

    ret = read(s, buffer, BUFFER_SIZE);
    if (ret < 1)
        syserr("read");

    int i = 0;
    while (i < ret && buffer[i] != '\n') {
        filename[i] = buffer[i];
        ++i;
    }
    filename[i++] = 0; // wliczamy znak nowej linii do przesłanych danych tu

    char size_str[256];
    memset(size_str, 0, sizeof(size_str));

    if (i >= ret) {
        ret = read(s, buffer, BUFFER_SIZE);
        if (ret < 1)
            syserr("read");
    }

    int j = 0;
    while (i < ret && buffer[i] != '\n') {
        size_str[j] = buffer[i];
        ++i; ++j;
    }
    size_str[i++] = 0; // wliczamy drugi znak nowej linii

    printf("new client %s size=%s file=%s\n", peername, size_str, filename);

    // Śpimy przez sekunde
    nanosleep(&millis_sec, &remain);

    FILE *received_file = fopen(filename, "w");
    if (!received_file) {
        syserr("file open");
    }

    unsigned long received_file_bytes = 0;

    if (i < ret) {
        // sklejona została też ramka z danymi z pliku, które musimy odebrać
        // i zapisać do pliku
        fwrite(buffer + i, sizeof(char), ret - i, received_file);
        received_file_bytes += ret - i;
    }

    char *endptr;
    unsigned long declared_file_size = strtoul(size_str, &endptr, 10);

    while (1) {
        memset(buffer, 0, sizeof(buffer));
        ret = read(s, buffer, sizeof(buffer));
        if (ret == -1) {
            syserr("read");
        }
        else if (ret == 0) {
            break;
        }
        fwrite(buffer, sizeof(char), ret, received_file);
        received_file_bytes += ret;
    }

    if (received_file_bytes != declared_file_size) {
        fatal("delared size: %ld, uploaded size: %ld\n", declared_file_size, received_file_bytes);
    }

    printf("client %s has sent its file of size=%ld\n", peername, received_file_bytes);
    if (pthread_mutex_lock(&lock) != 0) {
        syserr("lock failed");
    }
    total_upload += received_file_bytes;
    unsigned long local_total = total_upload;

    if (pthread_mutex_unlock(&lock) != 0) {
        syserr("unlock failed");
    }

    printf("total size of uploaded files %ld\n", local_total);

    if (fclose(received_file) != 0) {
        syserr("file  close");
    }
    if (close(s) != 0) {
        syserr("socket close");
    }
    return 0;
}

int main(int argc, char *argv[]) {
    int ear, rc;
    socklen_t len;
    struct sockaddr_in server;

    if (argc != 2) {
        fatal("Usage: %s port", argv[0]);
    }

    if (pthread_mutex_init(&lock, 0) != 0) {
        syserr("mutex init failed");
    }

    int port;
    sscanf(argv[1], "%d", &port);

    ear = socket(PF_INET, SOCK_STREAM, 0);
    if (ear == -1)
        syserr("socket");

    server.sin_family = AF_INET;
    server.sin_addr.s_addr = htonl(INADDR_ANY);
    server.sin_port = htons(port);
    rc = bind(ear, (struct sockaddr *) &server, sizeof(server));
    if (rc == -1) {
        syserr("bind");
    }


    /* Każdy chce wiedzieć jaki to port */
    len = (socklen_t) sizeof(server);
    rc = getsockname(ear, (struct sockaddr *) &server, &len);
    if (rc == -1) {
        syserr("getsockname");
    }

    printf("Listening at port %d\n", (int) ntohs(server.sin_port));

    rc = listen(ear, 5);
    if (rc == -1) {
        syserr("listen");
    }

    while(1) {
        int msgsock;
        int *con;
        pthread_t t;

        msgsock = accept(ear, (struct sockaddr *) NULL, NULL);
        if (msgsock == -1) {
            syserr("accept");
        }

        con = malloc(sizeof(int));
        if (!con) {
            syserr("malloc");
        }
        *con = msgsock;

        rc = pthread_create(&t, 0, handle_connection, con);
        if (rc == -1) {
            syserr("pthread_create");
        }

        /* No przecież nie będę na niego czekał ... */
        rc = pthread_detach(t);
        if (rc == -1) {
            syserr("pthread_detach");
        }
    }
}
