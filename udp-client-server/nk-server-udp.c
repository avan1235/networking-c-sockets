#include <netinet/in.h>
#include <stdio.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <unistd.h>

#include "err.h"

#define BUFFER_SIZE 10000
#define PORT_NUM 10001

#define FILE_NAME "buffer.data"

int main(int argc, char *argv[]) {
  int sock;
  int flags;
  struct sockaddr_in server_address;
  struct sockaddr_in client_address;

  char buffer[BUFFER_SIZE];
  socklen_t rcva_len;
  ssize_t len;

  sock = socket(AF_INET, SOCK_DGRAM, 0); // creating IPv4 UDP socket
  if (sock < 0)
    syserr("socket");
  // after socket() call; we should close(sock) on any execution path;
  // since all execution paths exit immediately, sock would be closed when
  // program terminates

  server_address.sin_family = AF_INET; // IPv4
  server_address.sin_addr.s_addr =
      htonl(INADDR_ANY); // listening on all interfaces
  server_address.sin_port =
      htons(PORT_NUM); // default port for receiving is PORT_NUM

  // bind the socket to a concrete address
  if (bind(sock, (struct sockaddr *)&server_address,
           (socklen_t)sizeof(server_address)) < 0)
    syserr("bind");

  for (;;) {
    do {
      rcva_len = (socklen_t)sizeof(client_address);
      flags = 0; // we do not request anything special
      len = recvfrom(sock, buffer, sizeof(buffer), flags,
                     (struct sockaddr *)&client_address, &rcva_len);
      if (len < 0)
        syserr("error on datagram from client socket");
      else {
        (void)printf("read from socket: %zd bytes\n", len);
        FILE *file = fopen(FILE_NAME, "a");
        if (file == NULL)
          syserr("fopen");

        if (len != fwrite(buffer, sizeof(char), len, file))
          syserr("fwrite");

        if (fclose(file))
          syserr("fclose");
      }
    } while (len > 0);
    (void)printf("finished exchange\n");
  }

  return 0;
}
