#include <netdb.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <unistd.h>

#include "err.h"

int main(int argc, char *argv[]) {
  int sock;
  struct addrinfo addr_hints;
  struct addrinfo *addr_result;

  int i, sflags;
  ssize_t snd_len;
  struct sockaddr_in my_address;
  socklen_t rcva_len;

  if (argc != 5) {
    fatal("Usage: %s host port n k\n", argv[0]);
  }

  // 'converting' host/port in string to struct addrinfo
  (void)memset(&addr_hints, 0, sizeof(struct addrinfo));
  addr_hints.ai_family = AF_INET; // IPv4
  addr_hints.ai_socktype = SOCK_DGRAM;
  addr_hints.ai_protocol = IPPROTO_UDP;
  addr_hints.ai_flags = 0;
  addr_hints.ai_addrlen = 0;
  addr_hints.ai_addr = NULL;
  addr_hints.ai_canonname = NULL;
  addr_hints.ai_next = NULL;
  if (getaddrinfo(argv[1], NULL, &addr_hints, &addr_result) != 0) {
    syserr("getaddrinfo");
  }

  uint16_t PORT = (uint16_t)atoi(argv[2]);
  uint32_t n = (uint32_t)atoi(argv[3]);
  uint32_t k = (uint32_t)atoi(argv[4]);

  my_address.sin_family = AF_INET; // IPv4
  my_address.sin_addr.s_addr = ((struct sockaddr_in *)(addr_result->ai_addr))
                                   ->sin_addr.s_addr; // address IP
  my_address.sin_port = htons(PORT); // port from the command line

  freeaddrinfo(addr_result);

  sock = socket(PF_INET, SOCK_DGRAM, 0);
  if (sock < 0)
    syserr("socket");

  char *send_data = (char *)malloc((k + 1) * sizeof(char));
  (void)memset(send_data, '$', k);
  send_data[k] = 0; // let's make a string from this allocated memory

  for (i = 0; i < n; i++) {
    (void)printf("sending to socket: %s\n", send_data);
    sflags = 0;
    rcva_len = (socklen_t)sizeof(my_address);
    snd_len = sendto(sock, send_data, k, sflags, (struct sockaddr *)&my_address,
                     rcva_len);
    if (snd_len != (ssize_t)k) {
      syserr("partial / failed write");
    }
  }

  if (close(sock) == -1) { // very rare errors can occur here, but then
    syserr("close");       // it's healthy to do the check
  }

  return 0;
}
